<?php

use App\Enums\MediaExtension;
use App\Enums\MediaFor;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->nullableMorphs('fileable');
            $table->text('url');
            $table->float('size')->nullable();
            $table->text('real_name')->nullable();
            $table->enum('for', MediaFor::getValues())->default(MediaFor::PHOTOGRAPH);
            $table->foreignId('extension_id')->references('id')->on('media_extensions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
