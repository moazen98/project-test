<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'Mohamad',
            'last_name' => 'Al Moazen',
            'email_verified_at' => now(),
            'basic_user' => 1,
            'remember_token' => Str::random(10),
            'image_url' => Config::get('custom_settings.image_employee_male_default'), //TODO: here default image for user
        ]);

        $user->attachRole('super_admin');

        //TODO: here morph for all user authentication management
        $user->authentication()->updateOrCreate([
            'email' => 'admin@gmail.com',
            'phone' => '0992875193',
            'international_code' => '+963',
            'password' => '123456', //TODO: here we use Mutators for password
            'is_active' => 1,
            'is_verified' => 1,
            'last_login_date' => now(),
        ]);


    }
}
