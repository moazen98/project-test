@extends('layouts.contentLayoutMaster')

@section('title', __('Show category'))

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <!-- vendor css files part2 -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/katex.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/monokai-sublime.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.bubble.css')) }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Inconsolata&family=Roboto+Slab&family=Slabo+27px&family=Sofia&family=Ubuntu+Mono&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">

@endsection

@section('page-style')
    {{-- Page Css files --}}


    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left card-title">{{ __('General Info') }}</h3>
                            <div class="float-right" @if(app()->getLocale() == 'en')style="margin-left: 600px"
                                 @else style="margin-right: 600px" @endif>

                                <div class="row">
                                    @permission('update_category')
                                    <div class="col-5">
                                        <a type="button" class="btn btn-primary"
                                           href="{{ route('category.edit',$category['id']) }}"><i
                                                class="fa fa-edit"></i></a>
                                    </div>
                                    @endpermission

                                    @permission('delete_category')
                                    <div class="col-5">
                                        <form method="post" id="myForm"
                                              action="{{route('category.destroy',$category['id'])}}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="button" class="btn btn-danger delete" title="{{__('Delete')}}"
                                            ><i class="fa fa-trash"></i></button>
                                        </form>
                                    </div>
                                    @endpermission

                                </div>

                            </div>


                        </div>
                        <div class="user-avatar-section">
                            <div class="d-flex align-items-center flex-column">
                                <div class="user-info text-center">
                                    <h4>{{$category['name']}}</h4>

                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="card-body">


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Name') }}:</h4>
                                    <p> {{ $category['name'] }} </p>
                                </div>

                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Parent') }}:</h4>
                                    <p> {{ $category['category_parent'] == null ? __('No parent') : $category['category_parent']['name'] }} </p>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Products number') }}:</h4>
                                    <span
                                        class="badge rounded-pill badge-light-success">{{$category['products_number']}}</span>
                                </div>
                            </div>


                        </div>
                    </div>


                    <div class="content-wrapper container-xxl p-0">
                        <div class="content-body">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="col-12">
                                                <h4 class="mt-2 mb-2 pt-50">{{__('Category products')}}</h4>

                                                <!-- Permission table -->
                                                <div class="card-datatable overflow-auto">
                                                    <table class="datatables-ajax table table-responsive ">
                                                        <thead>
                                                        <tr>
                                                            <th>{{__('ID')}}</th>
                                                            <th>{{__('Name')}}</th>
                                                            <th>{{__('Price')}}</th>
                                                            <th>{{__('Stock')}}</th>
                                                            <th>{{ __('Action') }}</th>
                                                        </tr>

                                                        <tbody>
                                                        @if($category['products'])
                                                            @foreach ($category['products'] as $index => $prod)
                                                                @foreach ($prod as $index => $product)

                                                                    <tr>
                                                                        <td>{{$product['id']}}</td>

                                                                        <td>{{$product['name']}}</td>


                                                                        <td>
                    <span
                        class="badge rounded-pill badge-light-info">{{$product['price']}}</span>
                                                                        </td>

                                                                        <td>
                    <span
                        class="badge rounded-pill badge-light-warning">{{$product['stock']}}</span>
                                                                        </td>

                                                                        <td>
                                                                            <div class="dropdown">
                                                                                <button type="button"
                                                                                        class="btn btn-sm dropdown-toggle hide-arrow py-0"
                                                                                        data-bs-toggle="dropdown">
                                                                                    <i data-feather="more-vertical"></i>
                                                                                </button>
                                                                                <div
                                                                                    class="dropdown-menu dropdown-menu-end">


                                                                                    @permission('read_product')
                                                                                    <a class="dropdown-item"
                                                                                       href="{{route('product.show',$product['id'])}}">
                                                                                        <i data-feather="eye"
                                                                                           class="me-50"></i>
                                                                                        <span>{{__('Show')}}</span>
                                                                                    </a>
                                                                                    @endpermission


                                                                                    @permission('delete_product')
                                                                                    <form method="post"
                                                                                          action="{{route('product.destroy',$product['id'])}}">
                                                                                        @csrf
                                                                                        @method('DELETE')
                                                                                        <button
                                                                                            class="btn delete delete-style dropdown-item">
                                                                                            <i data-feather="trash"
                                                                                               class="me-50"></i>
                                                                                            <span>{{__('Delete')}}</span>
                                                                                        </button>
                                                                                    </form>
                                                                                    @endpermission

                                                                                </div>
                                                                            </div>
                                                                        </td>


                                                                    </tr>
                                                                @endforeach
                                                            @endforeach
                                                        @endif
                                                        {{--<tr>--}}
                                                        </tbody>

                                                    </table>
                                                </div>
                                                <!-- Permission table -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>

@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>


    @include('admin.partials.scripts')

@endsection
