@foreach ($categories as $index => $category)
    <tr>
        <td>{{$category['id']}}</td>

        <td>{{$category['name']}}</td>


        <td>
                    <span
                        class="badge rounded-pill badge-light-info">{{$category['products_number']}}</span>
        </td>

        <td>
            <div class="dropdown">
                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    @permission('update_category')
                    <a class="dropdown-item" href="{{route('category.edit',$category['id'])}}">
                        <i data-feather="edit-2" class="me-50"></i>
                        <span>{{__('Edit')}}</span>
                    </a>
                    @endpermission


                    @permission('read_category')
                    <a class="dropdown-item" href="{{route('category.show',$category['id'])}}">
                        <i data-feather="eye" class="me-50"></i>
                        <span>{{__('Show')}}</span>
                    </a>
                    @endpermission


                    @permission('delete_category')
                    <form method="post"
                          action="{{route('category.destroy',$category['id'])}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn delete delete-style dropdown-item"><i data-feather="trash"
                                                                                 class="me-50"></i>
                            <span>{{__('Delete')}}</span></button>
                    </form>
                    @endpermission

                </div>
            </div>
        </td>


    </tr>
@endforeach
{{--<tr>--}}
<td colspan="3">
    {!! $categories->appends(request()->query())->links("pagination::bootstrap-4") !!}
</td>
{{--</tr>--}}

<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>

