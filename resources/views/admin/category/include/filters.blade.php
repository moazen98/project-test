<script>
    $(document).ready(function () {

        let timer;

        function fetch_data(page, sort_type, sort_by, query, page_counter) {
            $.ajax({
                url: "/admin/category/pagination/fetch-data?page=" + page + "&sortby=" + sort_by + "&sorttype=" + sort_type + "&query=" + query + "&page_counter=" + page_counter,
                success: function (data) {
                    $('tbody').html('');
                    $('tbody').html(data);
                }
            })
        }


        $(document).on('keyup', '#search', function () {

            clearTimeout(timer);
            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var page = $('#hidden_page').val();

            timer = setTimeout(function () {
                fetch_data(page, sort_type, column_name, query, page_counter);
            }, 1000);
        });

        $("#page_counter, #hidden_sort_type").change(function () {


            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var page = $('#hidden_page').val();

            fetch_data(page, sort_type, column_name, query, page_counter);
        });

        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            $('#hidden_page').val(page);
            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();

            $('li').removeClass('active');
            $(this).parent().addClass('active');
            fetch_data(page, sort_type, column_name, query, page_counter);
        });
    });
</script>
