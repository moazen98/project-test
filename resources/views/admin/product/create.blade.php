@extends('layouts.contentLayoutMaster')

@section('title', __('Add product'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">
    <link rel="stylesheet" href="{{asset('css/select-style.css')}}">


@endsection

@section('content')


    <section id="basic-horizontal-layouts">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Add Product to the System')}}</h4>
                </div>
                <div class="alert-body">
                    <!-- Vertical Wizard -->
                    <div class="card-body">
                        <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data"
                              id="regForm">
                            @csrf


                            <div class="mb-1">
                                <div class="row col-12 mb-2">
                                    @foreach (config('translatable.locales') as $locale)
                                        <div class="col-6">
                                            <label class="form-label"
                                                   for="basic-addon-name">{{ __('Name') }} {{__($locale)}} *</label>
                                            <input
                                                type="text"
                                                id="basic-addon-name"
                                                class="form-control"
                                                name="{{ $locale }}[name]"
                                                aria-describedby="basic-addon-name"
                                                value="{{ old($locale . '.name') }}" required
                                            />
                                            @error($locale . '.name')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    @endforeach

                                </div>

                                <div class="row col-12 mb-2">

                                    <div class="col-6">
                                        <label class="form-label" for="basic-addon-name">{{ __('Price') }}  *</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="price"
                                            aria-label="price"
                                            aria-describedby="basic-addon-name"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            value="{{old('price')}}" required
                                        />
                                        @error('price')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col-6">
                                        <label class="form-label" for="basic-addon-name">{{ __('Stock count') }} *</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="stock"
                                            aria-label="stock"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            aria-describedby="basic-addon-name"
                                            value="{{old('stock')}}" required
                                        />
                                        @error('stock')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>


                                </div>


                                <ul class="tree">

                                    @foreach($categories['categories'] as $category)

                                        <li>
                                            <input type="checkbox" name="category[]" value="{{$category['id']}}">
                                            <label>{{$category['name']}}</label>
                                            @if($category['children'])

                                                @foreach($category['children'] as $children)

                                                    <ul>
                                                        @foreach($children as $index => $child)
                                                            <li>
                                                                <input type="checkbox" name="category[]" value="{{$child['id']}}">
                                                                <label>{{$child['name']}}</label>


                                                                @if($child['children'])
                                                                    @if(count($child['children']) > 0)
                                                                        @include('admin.product.include.category-children',['childs' => $child['children']])
                                                                    @endif
                                                                @endif

                                                            </li>
                                                        @endforeach
                                                    </ul>

                                                @endforeach
                                            @endif
                                        </li>
                                    @endforeach


                                </ul>

                            </div>
                            @permission('create_product')
                            <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                            @endpermission
                        </form>
                    </div>
                    <!-- /Vertical Wizard -->
                </div>
            </div>
        </div>
    </section>

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
    <script src="{{asset('js/jquery.checktree.js')}}"></script>


    @include('admin.product.include.crud_script')

    @include('admin.partials.scripts')


@endsection





@endsection


