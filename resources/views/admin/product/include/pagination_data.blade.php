@foreach ($products as $index => $product)
    <tr>
        <td>{{$product['id']}}</td>

        <td>{{$product['name']}}</td>


        <td>
                    <span
                        class="badge rounded-pill badge-light-info">{{$product['price']}}</span>
        </td>

        <td>
                    <span
                        class="badge rounded-pill badge-light-warning">{{$product['stock']}}</span>
        </td>

        <td>
            <div class="dropdown">
                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">


                    @permission('read_product')
                    <a class="dropdown-item" href="{{route('product.show',$product['id'])}}">
                        <i data-feather="eye" class="me-50"></i>
                        <span>{{__('Show')}}</span>
                    </a>
                    @endpermission


                    @permission('delete_product')
                    <form method="post"
                          action="{{route('product.destroy',$product['id'])}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn delete delete-style dropdown-item"><i data-feather="trash"
                                                                                 class="me-50"></i>
                            <span>{{__('Delete')}}</span></button>
                    </form>
                    @endpermission

                </div>
            </div>
        </td>


    </tr>
@endforeach
{{--<tr>--}}
<td colspan="3">
    {!! $products->appends(request()->query())->links("pagination::bootstrap-4") !!}
</td>
{{--</tr>--}}

<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>

