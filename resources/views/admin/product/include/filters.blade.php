<script>
    $(document).ready(function () {

        let timer;

        function fetch_data(page, sort_type, sort_by, query, page_counter,category_id) {
            $.ajax({
                url: "/admin/product/pagination/fetch-data?page=" + page + "&sortby=" + sort_by + "&sorttype=" + sort_type + "&query=" + query + "&page_counter=" + page_counter  + "&category_id=" + category_id,
                success: function (data) {
                    $('tbody').html('');
                    $('tbody').html(data);
                }
            })
        }


        $(document).on('keyup', '#search', function () {

            clearTimeout(timer);
            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var page = $('#hidden_page').val();
            var category_id = $('#CATEGORY_ID').val();

            timer = setTimeout(function () {
                fetch_data(page, sort_type, column_name, query, page_counter,category_id);
            }, 1000);
        });

        $("#page_counter, #hidden_sort_type ,#CATEGORY_ID").change(function () {


            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var page = $('#hidden_page').val();
            var category_id = $('#CATEGORY_ID').val();

            fetch_data(page, sort_type, column_name, query, page_counter,category_id);
        });

        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            $('#hidden_page').val(page);
            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var category_id = $('#CATEGORY_ID').val();


            $('li').removeClass('active');
            $(this).parent().addClass('active');
            fetch_data(page, sort_type, column_name, query, page_counter,category_id);
        });
    });
</script>
