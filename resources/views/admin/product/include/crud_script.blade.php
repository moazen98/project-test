<script>
    $(document).ready(function () {

        $("#regForm").validate({
            rules: {

                price: {
                    required : true,
                    number: true
                },
                stock: {
                    required : true,
                    number: true
                },

            },
            messages: {
                "ar[name]": {
                    required: "{{trans('validation.required')}}",
                },
                "en[name]": {
                    required: "{{trans('validation.required')}}",
                },
                price: {
                    required: "{{trans('validation.required')}}",
                    number: "{{trans('validation.numeric')}}",
                },
                stock: {
                    required: "{{trans('validation.required')}}",
                    number: "{{trans('validation.numeric')}}",
                },
            }
        });
    });
</script>

<script>
    $(function () {
        $('ul.tree').checkTree({
            onExpand: null,
            onCollapse: null,
            onPreCheck: null,
            onCheck: null,
            onUnCheck: null,
            onLabelHoverOver: null,
            onLabelHoverOut: null
        });
    });
</script>
