<ul>
    @foreach($childs['categories'] as $index => $ch)
        <li>
            <input type="checkbox"
                   name="category[]" value="{{$ch['id']}}">
            <label>{{$ch['name']}}</label>

            @if($ch['children'])
                @if(count($ch['children']) > 0)
                    @include('admin.product.include.category-children',['childs' => $ch['children']])
                @endif
            @endif
        </li>
    @endforeach

</ul>
