@extends('layouts.contentLayoutMaster')

@section('title', __('Edit category'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">

@endsection





@section('content')
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

    <section id="basic-horizontal-layouts">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Add Category to the System')}}</h4>
                </div>
                <div class="alert-body">
                    <!-- Vertical Wizard -->
                    <div class="card-body">
                            <form action="{{ route('category.update',$category['id']) }}" method="post"
                                  enctype="multipart/form-data" id="regForm">
                                @csrf


                                <div class="mb-1">
                                    <div class="row col-12 mb-2">
                                        @foreach (config('translatable.locales') as $locale)
                                            <div class="col-6">
                                                <label class="form-label"
                                                       for="basic-addon-name">{{ __('Name') }} {{__($locale)}} *</label>
                                                <input
                                                    type="text"
                                                    id="basic-addon-name"
                                                    class="form-control"
                                                    name="{{ $locale }}[name]"
                                                    aria-describedby="basic-addon-name"
                                                    value=" {{ $category['category']->translate($locale)->name}}" required
                                                />
                                                @error($locale . '.name')
                                                <span class="text-danger">{{$message }}</span>
                                                @enderror
                                            </div>
                                        @endforeach


                                    </div>

                                    <div class="row col-12 mb-2">
                                        <div class="col-6">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="section">{{__('Category parent')}} </label>
                                                    <select class="select2 form-select" name="category_id">
                                                        <option selected disabled>{{__('Please select category')}}</option>
                                                        @foreach($categories['categories'] as $cat)
                                                            @if($category['category_parent'])
                                                                <option
                                                                    @if( $category['category_parent']['id'] == $cat['id']) selected
                                                                    @endif
                                                                    value="{{$cat['id']}}">{{$cat['name']}}</option>
                                                            @else
                                                                <option
                                                                    value="{{$cat['id']}}">{{$cat['name']}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @error('category_id')
                                                    <span class="text-danger">{{$message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                @permission('update_category')
                                <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                                @endpermission
                            </form>
                        </div>
                        <!-- /Vertical Wizard -->
                    </div>
                </div>
            </div>
        </section>

    @section('vendor-script')
        <!-- vendor files -->
        <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
        <!-- vendor files -->
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
        <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

    @endsection
    @section('page-script')
        <!-- Page js files -->
        <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>

    @include('admin.category.include.crud_script')

    @include('admin.partials.scripts')

@endsection


@endsection
