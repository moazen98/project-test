@extends('layouts/contentLayoutMaster')

@section('title', __('Edit Role'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">

@endsection



@section('content')
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

    @include('admin.user.role.include.styles')

    <div class="row">


        <section id="basic-horizontal-layouts">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{__('Edit Role in the System')}}</h4>
                    </div>
                    <div class="alert-body">
                        <!-- Vertical Wizard -->
                        <div class="card-body">
                            <form action="{{ route('role.update',$role->id) }}" method="post"
                                  enctype="multipart/form-data" id="regForm">
                                @csrf

                                @php
                                    $models = ['role','user','category','product','statistic'];
                                   $maps = ['create', 'read', 'update', 'delete'];
                                @endphp

                                <div class="modal-body px-5 pb-5">
                                    <div class="text-center mb-4">
                                        <h1 class="role-title">{{__('Edit Role')}}</h1>
                                        <p>{{__('Edit role permissions')}}</p>
                                    </div>
                                    <!-- Add role form -->
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="form-label" for="modalRoleName">{{__('Role Name Arabic')}}
                                                *</label>
                                            <input
                                                type="text"
                                                id="modalRoleName"
                                                name="name_ar"
                                                class="form-control"
                                                placeholder="{{__('Enter Role Name in Arabic')}}"
                                                value="{{$role->name_ar}}" required
                                            />
                                            @error('name_ar')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>

                                        <div class="col-6">
                                            <label class="form-label"
                                                   for="modalRoleName">{{__('Role Name English')}} *</label>
                                            <input
                                                type="text"
                                                id="modalRoleName"
                                                name="name"
                                                class="form-control"
                                                placeholder="{{__('Enter Role Name in English')}}"
                                                value="{{$role->name}}" required
                                            />
                                            @error('name')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-6">
                                            <label class="form-label"
                                                   for="modalRoleName">{{__('Role Description Arabic')}}</label>
                                            <input
                                                type="text"
                                                id="modalRoleName"
                                                name="description_ar"
                                                class="form-control"
                                                placeholder="{{__('Enter Role Description in Arabic')}}"
                                                data-msg="Please enter role name"
                                                value="{{$role->description_ar}}"
                                            />
                                            @error('description_ar')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>

                                        <div class="col-6">
                                            <label class="form-label"
                                                   for="modalRoleName">{{__('Role Description English')}}</label>
                                            <input
                                                type="text"
                                                id="modalRoleName"
                                                name="description"
                                                class="form-control"
                                                placeholder="{{__('Enter Role Description in English')}}"
                                                value="{{$role->description}}"
                                            />
                                            @error('description')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <h4 class="mt-2 pt-50">{{__('Role Permissions')}}</h4>
                                        <!-- Permission table -->
                                        <div class="table-responsive">
                                            <table class="table table-flush-spacing">
                                                <tbody>


                                                @foreach ($models as $index=>$model)

                                                    @php
                                                        $res = str_replace( array('_'), ' ', $model);
                                                    @endphp

                                                    <tr>
                                                        <td class="text-nowrap fw-bolder">{{__($res)}}</td>
                                                        <td>
                                                            @foreach ($maps as $map)
                                                                <div class="d-flex">
                                                                    <div class="form-check me-3 me-lg-5">
                                                                        <input class="form-check-input" type="checkbox"
                                                                               name="permissions[]"
                                                                               {{ $role->hasPermission($map . '_' . $model) ? 'checked' : '' }}
                                                                               id="userManagementRead"
                                                                               value="{{ $map . '_' . $model }}"
                                                                        />
                                                                        <label class="form-check-label"
                                                                               for="userManagementRead">
                                                                            {{__($map)}} </label>
                                                                    </div>
                                                                    @endforeach
                                                                </div>
                                                        </td>
                                                    </tr>

                                                @endforeach

{{--                                                @foreach ($modelsRead as $index=>$model)--}}
{{--                                                    <tr>--}}
{{--                                                        @php--}}
{{--                                                            $res = str_replace( array('_'), ' ', $model);--}}
{{--                                                        @endphp--}}
{{--                                                        <td class="text-nowrap fw-bolder">{{__($res)}}</td>--}}
{{--                                                        <td>--}}
{{--                                                            @foreach ($readMaps as $map)--}}
{{--                                                                <div class="d-flex">--}}
{{--                                                                    <div class="form-check me-3 me-lg-5">--}}
{{--                                                                        <input class="form-check-input" type="checkbox"--}}
{{--                                                                               name="permissions[]"--}}
{{--                                                                               id="userManagementRead"--}}
{{--                                                                               {{ $role->hasPermission($map . '_' . $model) ? 'checked' : '' }}--}}
{{--                                                                               value="{{ $map . '_' . $model }}"--}}
{{--                                                                        />--}}
{{--                                                                        <label class="form-check-label"--}}
{{--                                                                               for="userManagementRead">--}}
{{--                                                                            {{__($map)}} </label>--}}
{{--                                                                    </div>--}}
{{--                                                                    @endforeach--}}
{{--                                                                </div>--}}
{{--                                                        </td>--}}
{{--                                                    </tr>--}}

{{--                                                @endforeach--}}

                                                @error('permissions')
                                                <span class="text-danger">{{$message }}</span>
                                                @enderror

                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- Permission table -->
                                    </div>
                                    <div class="col-12 text-center mt-2">
                                        @permission('update_role')
                                        <button type="submit" class="btn btn-primary me-1">{{__('Submit')}}</button>
                                        @endpermission
                                        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                                aria-label="Close">
                                            {{__('Discard')}}
                                        </button>
                                    </div>
                                    <!--/ Add role form -->
                                </div>
                            </form>
                        </div>
                        <!-- /Vertical Wizard -->
                    </div>
                </div>
            </div>
        </section>
    @endsection
    @section('vendor-script')
        <!-- vendor files -->
            <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
            <!-- vendor files -->
            <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
            <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
            <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

    @endsection
    @section('page-script')
        <!-- Page js files -->
            <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>



@endsection


@include('admin.user.role.include.scripts')

