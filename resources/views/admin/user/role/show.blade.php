@extends('layouts/contentLayoutMaster')

@section('title', __('Show Role'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">

@endsection


<style>
    select2-container--default {
        display: flex !important;
    }
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>



@section('content')
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <style>
        .form-switch .form-check-label .switch-text-left i, .form-switch .form-check-label .switch-text-left svg, .form-switch .form-check-label .switch-text-right i, .form-switch .form-check-label .switch-text-right svg, .form-switch .form-check-label .switch-icon-left i, .form-switch .form-check-label .switch-icon-left svg, .form-switch .form-check-label .switch-icon-right i, .form-switch .form-check-label .switch-icon-right svg {
            height: 13px;
            width: 13px;
            margin-top: 6px;
            font-size: 13px;
        }
    </style>
    <div class="row">


        <section id="basic-horizontal-layouts">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{__('Edit Role in the System')}}</h4>
                    </div>
                    <div class="alert-body">
                        <!-- Vertical Wizard -->
                        <div class="card-body">
                            @csrf
                            @php
                                $models = ['role','user','category','product','statistic'];
                             $maps = ['create', 'read', 'update', 'delete'];
                            @endphp

                            <div class="modal-body px-5 pb-5">
                                <div class="text-center mb-4">
                                    <h1 class="role-title">{{__('Show Role')}}</h1>
                                    <p>{{__('Show role permissions')}}</p>
                                </div>
                                <!-- Add role form -->
                                <div class="row">
                                    <div class="col-6">
                                        <label class="form-label" for="modalRoleName">{{__('Role Name Arabic')}}</label>
                                        <input
                                            type="text"
                                            id="modalRoleName"
                                            name="name_ar"
                                            class="form-control"
                                            placeholder="{{__('Enter role name in arabic')}}"
                                            tabindex="-1"
                                            data-msg="Please enter role name"
                                            value="{{$role->name_ar}}"
                                            readonly
                                            disabled
                                        />
                                        @error('name_ar')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col-6">
                                        <label class="form-label"
                                               for="modalRoleName">{{__('Role Name English')}}</label>
                                        <input
                                            type="text"
                                            id="modalRoleName"
                                            name="name"
                                            class="form-control"
                                            placeholder="{{__('Enter role name in english')}}"
                                            tabindex="-1"
                                            data-msg="Please enter role name"
                                            value="{{$role->name}}"
                                            readonly
                                            disabled
                                        />
                                        @error('name')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-6">
                                        <label class="form-label"
                                               for="modalRoleName">{{__('Role Description Arabic')}}</label>
                                        <input
                                            type="text"
                                            id="modalRoleName"
                                            name="description_ar"
                                            class="form-control"
                                            placeholder="{{__('Enter role description in arabic')}}"
                                            tabindex="-1"
                                            data-msg="Please enter role name"
                                            value="{{$role->description_ar}}"
                                            readonly
                                            disabled
                                        />
                                        @error('description_ar')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col-6">
                                        <label class="form-label"
                                               for="modalRoleName">{{__('Role Description English')}}</label>
                                        <input
                                            type="text"
                                            id="modalRoleName"
                                            name="description"
                                            class="form-control"
                                            placeholder="{{__('Enter role description in english')}}"
                                            tabindex="-1"
                                            data-msg="Please enter role name"
                                            value="{{$role->description}}"
                                            readonly
                                            disabled
                                        />
                                        @error('description')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12">
                                    <h4 class="mt-2 pt-50">{{__('Role Permissions')}}</h4>
                                    <!-- Permission table -->
                                    <div class="table-responsive">
                                        <table class="table table-flush-spacing">
                                            <tbody>
                                            @foreach ($models as $index=>$model)

                                                @php
                                                    $res = str_replace( array('_'), ' ', $model);
                                                @endphp

                                                <tr>
                                                    <td class="text-nowrap fw-bolder">{{__($res)}}</td>
                                                    <td>
                                                        @foreach ($maps as $map)
                                                            <div class="d-flex">
                                                                <div class="form-check me-3 me-lg-5">
                                                                    <input class="form-check-input" type="checkbox"
                                                                           name="permissions[]"
                                                                           {{ $role->hasPermission($map . '_' . $model) ? 'checked' : '' }}
                                                                           id="userManagementRead"
                                                                           value="{{ $map . '_' . $model }}"
                                                                           readonly
                                                                           disabled
                                                                    />
                                                                    <label class="form-check-label"
                                                                           for="userManagementRead">
                                                                        {{__($map)}} </label>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                    </td>
                                                </tr>

                                            @endforeach



                                            @foreach ($modelsRead as $index=>$model)

                                                @php
                                                    $res = str_replace( array('_'), ' ', $model);
                                                @endphp

                                                <tr>
                                                    <td class="text-nowrap fw-bolder">{{__($res)}}</td>
                                                    <td>
                                                        @foreach ($readMaps as $map)
                                                            <div class="d-flex">
                                                                <div class="form-check me-3 me-lg-5">
                                                                    <input class="form-check-input" type="checkbox"
                                                                           name="permissions[]"
                                                                           {{ $role->hasPermission($map . '_' . $model) ? 'checked' : '' }}
                                                                           id="userManagementRead"
                                                                           value="{{ $map . '_' . $model }}"
                                                                           readonly
                                                                           disabled
                                                                    />
                                                                    <label class="form-check-label"
                                                                           for="userManagementRead">
                                                                        {{__($map)}} </label>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                    </td>
                                                </tr>

                                            @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Permission table -->
                                </div>
                                <!--/ Add role form -->
                            </div>
                    </form>
                    </div>
                    <!-- /Vertical Wizard -->
                </div>
            </div>
    </div>
    </section>
@endsection
@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>



@endsection

<style type="text/css">

    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }

</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput-jquery.js"></script>
<script src="{{asset('build/js/intlTelInput.js')}}"></script>



