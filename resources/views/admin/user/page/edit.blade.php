@extends('layouts.contentLayoutMaster')

@section('title', __('Edit User'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">

@endsection





@section('content')
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

    <div class="row">


        <section id="basic-horizontal-layouts">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{__('Edit User in the System')}}</h4>
                    </div>
                    <div class="alert-body">
                        <!-- Vertical Wizard -->
                        <div class="card-body">
                            <form action="{{ route('user.update',$user['id']) }}" method="post"
                                  enctype="multipart/form-data" id="regForm2">
                                @csrf

                                <input type="hidden" name="auth_id" value="{{$user['auth_id']}}" readonly>

                                <div class="mb-1">
                                    <div class="row col-12 mb-2">
                                        <div class="col-6">
                                            <label class="form-label" for="basic-addon-name">{{ __('First Name') }}
                                                *</label>
                                            <input
                                                type="text"
                                                id="basic-addon-name"
                                                class="form-control"
                                                name="first_name"
                                                aria-label="Full name"
                                                aria-describedby="basic-addon-name"
                                                value="{{$user['first_name']}}" required
                                            />
                                            @error('first_name')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-6">
                                            <label class="form-label" for="basic-addon-name">{{ __('Last Name') }}
                                                *</label>
                                            <input
                                                type="text"
                                                id="basic-addon-name"
                                                class="form-control"
                                                name="last_name"
                                                aria-label="last_name"
                                                aria-describedby="basic-addon-name"
                                                value="{{$user['last_name']}}" required
                                            />
                                            @error('last_name')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>





                                    <div class="row col-12 mb-2">
                                        <div class="col-6">
                                            <input type="hidden" id="code" name="international_code" value="{{$user['international_code']}}"/>
                                            <label class="form-label" for="phone">{{ __('Phone') }} *</label>
                                            <br>
                                            <input class="form-control" id="phone" name="phone_number" type="tel"
                                                   oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                   value="{{$user['international_code'].$user['phone']}}" required>
                                            @error('phone_number')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>

                                        <div class="col-6">
                                            <label class="form-label" for="basic-addon-name">{{ __('Email') }} *</label>
                                            <input
                                                type="email"
                                                id="basic-addon-name"
                                                class="form-control"
                                                name="email"
                                                aria-label="email"
                                                aria-describedby="basic-addon-name"
                                                value="{{$user['email']}}" required
                                            />
                                            @error('email')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row col-12 mb-2">

                                        <div class="col-6">
                                            <div class="d-flex justify-content-between">
                                                <label class="form-label" for="login-password">{{__('Password')}} (
                                                    <small>{{__(' Fill it when need to change password ')}}</small>)</label>


                                            </div>
                                            <div class="input-group input-group-merge form-password-toggle">
                                                <input
                                                    type="password"
                                                    class="form-control form-control-merge"
                                                    id="password"
                                                    name="password"
                                                    placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                    aria-describedby="login-password"
                                                />
                                                <span class="input-group-text cursor-pointer"><i
                                                        data-feather="eye"></i></span>
                                            </div>
                                            @error('password')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>

                                        <div class="col-6">
                                            <div class="d-flex justify-content-between">
                                                <label class="form-label"
                                                       for="login-password">{{__('Confirm Password')}} (
                                                    <small>{{__(' Fill it when need to change password ')}}</small>)</label>


                                            </div>
                                            <div class="input-group input-group-merge form-password-toggle">
                                                <input
                                                    type="password"
                                                    class="form-control form-control-merge"
                                                    id="login-password"
                                                    name="confirm_password"
                                                    placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                    aria-describedby="login-password"
                                                />
                                                <span class="input-group-text cursor-pointer"><i
                                                        data-feather="eye"></i></span>
                                            </div>
                                            @error('confirm_password')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>







                                    <div class="row col-12 mb-2">
                                        <div class="col-6">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="section">{{__('Employee Role')}} *</label>
                                                    <select class="select2 form-select" name="role" required>
                                                        @if(!is_null($user['role']))
                                                            @foreach($roles['roles'] as $role)

                                                                    <option
                                                                        @if($user['role_id'] == $role['id']) selected
                                                                        @endif
                                                                        value="{{$role['id']}}">{{$role['name']}}</option>

                                                            @endforeach
                                                        @else
                                                            @foreach($roles['roles'] as $role)

                                                                    <option
                                                                        value="{{$role['id']}}">{{$role['name']}}</option>

                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    @error('section')
                                                    <span class="text-danger">{{$message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-2">
                                        <div class="col-12">
                                            <!-- remove thumbnail file upload starts -->
                                            <label class="form-label" for="image">{{ __('Employee Image') }}</label>
                                            <div class="card-body">
                                                <input class="form-control" id="uploadInputImage" type="file"
                                                       name="image"
                                                       accept="image/png, image/gif, image/jpeg"/>
                                                @error('image')
                                                <span class="text-danger">{{$message }}</span>
                                                @enderror
                                                <img class="img-fluid card-img-top"
                                                     style="width: 129px; margin-top: 2%;height: 100px"
                                                     src="{{$user['image_path']}}" id="uploadInputPreview"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-6">
                                            <div style="margin-left: 3%;" class="mb-2 form-check form-switch">
                                                <input type="checkbox" class="form-check-input" name="is_active" value="1"
                                                       id="customSwitch1" @if($user['active'] == 1) checked @endif/>
                                                <label class="form-check-label"
                                                       for="customSwitch1">{{ __('Activate') }}</label>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                                @permission('update_user')
                                <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                                @endpermission
                            </form>
                        </div>
                        <!-- /Vertical Wizard -->
                    </div>
                </div>
            </div>
        </section>

    @section('vendor-script')
        <!-- vendor files -->
            <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
            <!-- vendor files -->
            <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
            <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
            <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

    @endsection
    @section('page-script')
        <!-- Page js files -->
            <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>


    @include('admin.user.page.include.phone_script')

    @include('admin.user.page.include.crud_script')

    @include('admin.partials.scripts')

@endsection


@endsection
