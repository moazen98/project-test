<script>
    $(document).ready(function () {

        //delete
        $('.delete').click(function (e) {

            var that = $(this)

            e.preventDefault();

            var n = new swal({
                title: '{{__('Are you sure you want to delete this record?')}}',
                text: "{{__('If you delete this, it will be gone forever.')}}",
                icon: "warning",
                // buttons: true,
                showCancelButton: true,
                showConfirmButton: true,
                dangerMode: true,
                type: "danger",
                closeOnConfirm: false,
                confirmButtonClass: "btn-danger",
            }).then((willDelete) => {
                if (willDelete['isConfirmed'] == true) {
                    that.closest('form').submit();
                } else {
                    // n.close();
                }
            });

        });//end of delete
    });//end of ready

</script>
