
<script>
    $('document').ready(function () {
        $("#uploadInputImage").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadInputPreview').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>


<script>
    $(document).ready(function () {

        //delete
        $('.delete').click(function (e) {

            var that = $(this)

            e.preventDefault();

            var n = new swal({
                title: '{{__('Are you sure you want to delete this record?')}}',
                text: "{{__('If you delete this, it will be gone forever.')}}",
                icon: "warning",
                // buttons: true,
                showCancelButton: true,
                showConfirmButton: true,
                dangerMode: true,
                type: "danger",
                closeOnConfirm: false,
                confirmButtonClass: "btn-danger",
            }).then((willDelete) => {
                if (willDelete['isConfirmed'] == true) {
                    that.closest('form').submit();
                } else {
                    // n.close();
                }
            });

        });//end of delete
    });//end of ready

</script>


<script>
    var input = document.querySelector("#phone");

    $("#phone").intlTelInput({
        hiddenInput: "full",
        initialCountry: "tr",
        separateDialCode: true,
        formatOnDisplay: false,
        utilsScript: "{{asset('build/js/utils.js')}}",
    }).on("countrychange", function() {
        $("#code").val("+"+($("#phone").intlTelInput("getSelectedCountryData").dialCode));
    });

</script>


<script>
    var input2 = document.querySelector("#phone2");

    $("#phone2").intlTelInput({
        hiddenInput: "full2",
        initialCountry: "tr",
        separateDialCode: true,
        formatOnDisplay: false,
        utilsScript: "{{asset('build/js/utils.js')}}",
    }).on("countrychange", function() {
        $("#code2").val("+"+($("#phone2").intlTelInput("getSelectedCountryData").dialCode));
    });

</script>

<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>


<script>
    CKEDITOR.config.language = "{{ app()->getLocale() }}";
</script>
