@extends('layouts/contentLayoutMaster')

@section('title', __('Statistics'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
@endsection

@section('content')

    <!-- apex charts section start -->
    <section id="apexchart">
        <div class="row">


            <!-- Bar Chart Starts -->
            <div class="col-xl-12 col-12">
                <div class="card">
                    <div
                        class="
            card-header
            d-flex
            flex-sm-row flex-column
            justify-content-md-between
            align-items-start
            justify-content-start
          "
                    >
                        <div>
                            <p class="card-subtitle text-muted mb-25">{{__('Products number')}}</p>
                            <h4 class="card-title fw-bolder">{{count($products['products'])}}</h4>
                        </div>

                    </div>
                    <div class="card-body">
                        <div id="bar-chart"></div>
                    </div>
                </div>
            </div>
            <!-- Bar Chart Ends -->


            <!-- Apex charts section end -->
        </div>
    </section>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/charts/chart-apex.js')) }}"></script>



    <script>

        isRtl = $('html').attr('data-textdirection') === 'rtl';

        var resultName = @json($resultName);
        var resultNumber = @json($resultNumber);

        // Bar Chart
        // --------------------------------------------------------------------
        var barChartEl = document.querySelector('#bar-chart'),
            barChartConfig = {
                chart: {
                    height: 200,
                    type: 'bar',
                    parentHeightOffset: 0,
                    toolbar: {
                        show: false
                    }
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        barHeight: '30%',
                        endingShape: 'rounded'
                    }
                },
                grid: {
                    xaxis: {
                        lines: {
                            show: false
                        }
                    },
                    padding: {
                        top: -15,
                        bottom: -10
                    }
                },
                colors: window.colors.solid.info,
                dataLabels: {
                    enabled: false
                },
                series: [
                    {
                        data: resultNumber
                    }
                ],
                xaxis: {

                    categories:resultName

                },
                yaxis: {
                    opposite: isRtl
                }
            };
        if (typeof barChartEl !== undefined && barChartEl !== null) {
            var barChart = new ApexCharts(barChartEl, barChartConfig);
            barChart.render();
        }
    </script>

@endsection
