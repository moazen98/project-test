<?php

return [

    "Users" => 'المستخدمين',
    "Dashboards" => "الواجهة الرئيسية",
    "Employees" => 'الموظفين',
    "Roles" => 'الصلاحيات',
    "System Users" => 'مستخدمين النظام',
    "Categories/Products" => 'التصنيقات/المنتجات',
    "Statistics" => 'الإحصائيات',
    "Products" => 'المنتجات',
    "Categories" => 'التصنيفات'

];
