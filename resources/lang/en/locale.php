<?php

return [


    "Users" => 'Users',
    "Dashboards" => "Dashboards",
    "Employees" => 'Employees',
    "Roles" => 'Roles',
    "System Users" => 'System Users',
    "Categories/Products" => 'Categories/Products',
    "Statistics" => 'Statistics',
    "Products" => 'Products',
    "Categories" => 'Categories'
];
