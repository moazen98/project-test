<?php


return [
    'dashboard_paginate' => 10,
    'api_paginate' => 10,
    'media_company' => 'storage'.'/'.'agency'.'/',
    'image_company_default' => 'company-default.png',
    'image_employee_male_default' => 'employee_male_default.jpg',
    'image_user_default_url' => 'storage/user/',
];
