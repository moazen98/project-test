<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $users = User::all();

        foreach ($users as $index => $user) {
            $data[$index]['id'] = $user->id;
            $data[$index]['first_name'] = $user->first_name;
            $data[$index]['last_name'] = $user->last_name;
            $data[$index]['phone'] = $user->authentication == null ? null : ($user->authentication->international_code . $user->authentication->phone);
            $data[$index]['email'] = $user->authentication == null ? null : $user->authentication->email;
            $data[$index]['active'] = $user->authentication == null ? null : ($user->authentication->is_active == 1 ? __('Active') : __('InActivate'));
            $data[$index]['date'] = $user->created_at->format('m-d-Y');

        }

        return $data;
    }

    public function headings(): array
    {
        return [__("Id"), __("First Name"), __("Last Name"), __("Phone"), __("Email"), __("Status"), __("Date")];
    }
}
