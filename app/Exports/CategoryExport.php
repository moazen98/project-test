<?php

namespace App\Exports;

use App\Models\Category\Category;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CategoryExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $categories = Category::all();

        foreach ($categories as $index => $category) {
            $data[$index]['id'] = $category->id;
            $data[$index]['name'] = $category->name;
            $data[$index]['parent'] = $category->parent == null ? __('No parent') : $category->parent->name;
            $data[$index]['number'] = $category->products()->count();
            $data[$index]['date'] = $category->created_at->format('m-d-Y');

        }

        return $data;
    }

    public function headings(): array
    {
        return [__("Id"), __("Name"), __("Parent"), __("Products number"), __("Date")];
    }
}
