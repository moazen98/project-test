<?php

namespace App\Exports;

use App\Models\Category\Category;
use App\Models\Product\Product;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $products = Product::all();

        foreach ($products as $index => $product) {
            $data[$index]['id'] = $product->id;
            $data[$index]['name'] = $product->name;
            $data[$index]['number'] = $product->categories()->count();
            $data[$index]['date'] = $product->created_at->format('m-d-Y');

        }

        return $data;
    }

    public function headings(): array
    {
        return [__("Id"), __("Name"), __("Category number"), __("Date")];
    }
}
