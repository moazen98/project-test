<?php

namespace App\Exports;

use App\Models\Company;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CompanyExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $users = Company::all();

        foreach ($users as $index => $user) {
            $data[$index]['id'] = $user->id;
            $data[$index]['name'] = $user->name;
            $data[$index]['email'] = $user->email;
            $data[$index]['website'] = $user->website;
            $data[$index]['number'] = $user->employees()->count();
            $data[$index]['date'] = $user->created_at->format('m-d-Y');

        }

        return $data;
    }

    public function headings(): array
    {
        return [__("Id"), __("Name"), __("Email"), __("Website"), __("Employees number"), __("Date")];
    }
}
