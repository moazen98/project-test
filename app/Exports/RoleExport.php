<?php

namespace App\Exports;

use App\Models\Role;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RoleExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $roles = Role::all();

        foreach ($roles as $index => $role) {
            $data[$index]['id'] = ++$index;
            $data[$index]['name_ar'] = $role->name;
            $data[$index]['name_en'] = $role->name_ar;
            $data[$index]['display_name'] = $role->display_name;
            $data[$index]['description'] = $role->description;
            $data[$index]['description_ar'] = $role->description_ar;
            $data[$index]['date'] = $role->created_at->format('m-d-Y');
        }

        return $data;
    }

    public function headings(): array
    {
        return [__("Id"), __("Name Arabic"), __("Name English"), __("Display Name"), __("Description English"),__("Description Arabic"),__("Date")];
    }
}
