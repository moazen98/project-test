<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class CheckOperatorStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:checkOperatorStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user) {
//            if ($user->section()->whereIn('type',['doctor','laser','facecare','injection','slimming','doctor_injection'])){
            if ($user->section->type == 'laser' || $user->section->type == 'doctor' || $user->section->type == 'facecare'
                || $user->section->type == 'injection' || $user->section->type == 'slimming' || $user->section->type == 'doctor_injection') {
                $userOrders = count($user->ordersOperator()->where('is_finish', 0)->get());
                if ($userOrders == 0) {
                    $user->is_busy = 0;
                    $user->save();
                } else {
                    $user->is_busy = 1;
                    $user->save();
                }
            }
        }
        return 0;
    }
}
