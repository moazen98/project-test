<?php

namespace App\Models;

use App\Enums\MediaFor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laratrust\Traits\LaratrustUserTrait;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use SoftDeletes;
    use HasApiTokens;

    protected $table = 'users';


    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'basic_user',
        'image_url'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'image_path',
        'name',
        'full_phone'
    ];


    //TODO: here we use set attribute for password field
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }


    public function authentication()
    {
        return $this->morphOne(Authentication::class, 'authenticatable');

    }


    public function getImagePathAttribute()
    {
        return asset(Config::get('custom_settings.image_user_default_url') . $this->image_url);

    }//end of get image path


    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;

    }


    public function getFullPhoneAttribute()
    {
        return $this->authentication->international_code . ' ' . $this->authentication->phone;

    }

    public function files()
    {
        return $this->morphOne(File::class, 'fileable');
    }


    public function setImage_urlAttributes(Request $request)
    {
        if ($request->has('image')) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path(Config::get('custom_settings.image_user_default_url') . $request->image->hashName()));

            $this->attributes['image_url'] = $request->image->hashName();
        } else {

            $this->attributes['image_url'] =  Config::get('custom_settings.image_employee_male_default');

        }
    }
}
