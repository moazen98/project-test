<?php

namespace App\Models\Category;

use App\Models\Product\Product;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, softDeletes, Translatable;

    protected $fillable = [
        'category_id',
    ];

    public $translatedAttributes = [
        'name',
    ];

    public function categories()
    {
        return $this->hasMany(Category::class, 'category_id', 'id');
    }
//
//    public function myChildren() {
//        return $this->children()->where('category_id','=', $this->id);
//    }

    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category');
    }

    public function setTranslatedAttributes($request)
    {
        foreach (config('translatable.locales') as $locale) {
            if ($request->has($locale)) {
                $this->getTranslationOrNew($locale)->name = $request->get($locale)['name'];
            }

        }
    }
}
