<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Authentication extends Model
{
    use HasFactory;
    protected $table = 'authentications';
    protected $fillable = ['email','phone','international_code','password','is_active','is_verified','last_login_date'];

    protected $dates = ['last_login_date'];


    public function authenticatable(){
        return $this->morphTo('authenticatable');
    }



    //TODO: here we use set attribute for password field
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }
}
