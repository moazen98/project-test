<?php

namespace App\Models\Product;

use App\Models\Category\Category;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, softDeletes, Translatable;

    protected $table = 'products';

    protected $fillable = [
        'price',
        'stock',
    ];

    public $translatedAttributes = [
        'name',
    ];


    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function setTranslatedAttributes($request)
    {
        foreach (config('translatable.locales') as $locale) {
            if ($request->has($locale)) {
                $this->getTranslationOrNew($locale)->name = $request->get($locale)['name'];
            }

        }
    }
}
