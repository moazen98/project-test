<?php

namespace App\Providers;

use App\Event\NotificationAllUserStoreEvent;
use App\Listeners\NotificationAllUserStoreListener;
use App\Models\Agency\Branch;
use App\Models\Agency\Dealership;
use App\Models\Agency\Office;
use App\Models\Category\Category;
use App\Models\Offer;
use App\Models\Operation;
use App\Models\Product\Product;
use App\Models\TransferIncoming;
use App\Models\TransferOutgoing;
use App\Models\User;
use App\Observers\BranchObserver;
use App\Observers\CategoryObserver;
use App\Observers\DealershipObserver;
use App\Observers\EmployeeObserver;
use App\Observers\IncomingTransferObserver;
use App\Observers\OfferObserver;
use App\Observers\OfficeObserver;
use App\Observers\OutgoingTransferObserver;
use App\Observers\ProductObserver;
use App\Observers\SaleObserver;
use App\Observers\UserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Product::observe(ProductObserver::class);
        Category::observe(CategoryObserver::class);
    }
}
