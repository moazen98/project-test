<?php

namespace App\Http\Controllers;

use App\Models\Category\Category;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Product\Product;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
  // Dashboard - Analytics
  public function dashboardAnalytics()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/dashboard/dashboard-analytics', ['pageConfigs' => $pageConfigs]);
  }

  // Dashboard - Ecommerce
  public function dashboardEcommerce()
  {
    $pageConfigs = ['pageHeader' => false];

    $users = User::count();
    $categories = Category::count();
    $products = Product::count();
    $roles = Role::count();

    return view('/content/dashboard/dashboard-ecommerce', ['pageConfigs' => $pageConfigs,'users' => $users , 'categories' => $categories, 'products' => $products,'roles'=> $roles]);
  }
}
