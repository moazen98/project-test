<?php

namespace App\Http\Controllers\Admin\Authentication;

use App\Enums\AccountStatus;
use App\Enums\AgencyType;
use App\Enums\UserAuthenticationType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auth\LoginStoreRequest;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{


    public function login(LoginStoreRequest $request)
    {
        $method = inputCredentialType($request);

        if ($method == UserAuthenticationType::EMAIL) {
            $status = app('servicesV1')->authenticationService->loginViaEmail($request->email, $request->password);
        } elseif ($method == UserAuthenticationType::PHONE) {
            $status = app('servicesV1')->authenticationService->loginViaPhone($request->phone, $request->password);
        } else {
            return redirect()->route('admin.get.login')
                ->with('failed', __('message.login_username_password_wrong'));
        }


        if ($status == 1) {
            return redirect()->route('admin.dashboard-ecommerce')
                ->with('success', __('message.login_success'));
        }

        if ($status == AccountStatus::NOT_ACTIVE) {
            return redirect()->route('admin.get.login')
                ->with('failed', __('message.login_active_failed'));
        }

        if ($status == AccountStatus::NOT_VERIFY) {
            return redirect()->route('admin.get.login')
                ->with('failed', __('message.login_verify_failed'));
        }

        if (!$status) {
            return redirect()->route('admin.get.login')
                ->with('failed', __('message.login_username_password_wrong'));
        }

        return redirect()->route('admin.get.login')
            ->with('failed', __('message.login_username_password_wrong'));

    }


    public function getAdminLogin()
    {

        if (Auth::check()) {

            return redirect()->route('admin.dashboard-ecommerce');

        } else {

            return view('admin.auth.login');
        }

    }


    public function logout()
    {

        $status = app('servicesV1')->authenticationService->logout();

        if ($status == AgencyType::USERS) {
            return redirect()->route('admin.get.login');
        }

        return redirect()->back()->with('failed', __('message.failed'));


    }

    public function index()
    {

        if (Auth::check()) {
            return redirect()->route('admin.dashboard-ecommerce');
        } else {
            return redirect()->route('admin.get.login');
        }
    }
}
