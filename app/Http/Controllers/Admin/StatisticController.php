<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Dashboard\Product\ProductCollection;
use App\Http\Resources\Dashboard\Product\ProductResource;
use Illuminate\Http\Request;

class StatisticController extends MainDashboardController
{

    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_statistic'])->only('index');

    }//end of constructor


    public function index(){

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Statistic Products')]
        ];


        $productsResult = app('servicesV1')->productService->getAllProducts()->get();

        $products = (new ProductCollection($productsResult))->toArray();


        $resultName = app('servicesV1')->productService->getProductName($productsResult);
        $resultNumber = app('servicesV1')->productService->getProductNumber($productsResult);


        return view('admin.statistic.index',compact('products','resultName','resultNumber','breadcrumbs'));
    }
}
