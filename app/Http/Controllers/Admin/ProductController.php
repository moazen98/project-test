<?php

namespace App\Http\Controllers\Admin;

use App\Exports\CategoryExport;
use App\Exports\ProductExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\CategoryStoreRequest;
use App\Http\Requests\Admin\Category\CategoryUpdateRequest;
use App\Http\Requests\Admin\Product\ProductStoreRequest;
use App\Http\Resources\Dashboard\Category\CategoryCollection;
use App\Http\Resources\Dashboard\Category\CategoryResource;
use App\Http\Resources\Dashboard\Product\ProductResource;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_product'])->only('index');
        $this->middleware(['permission:create_product'])->only('create');
        $this->middleware(['permission:update_product'])->only('edit');
        $this->middleware(['permission:delete_product'])->only('destroy');
        $this->middleware(['permission:read_product'])->only('export');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Products')]
        ];


        $products = app('servicesV1')->productService->getAllProducts()->paginate($this->dashboardPaginate);

        $products->getCollection()->transform(function ($item) use ($request) {
            return (new ProductResource($item))->toArray($request);
        });

        $categoriesResult = app('servicesV1')->categoryService->getAllCategories()->get();

        $categories = (new CategoryCollection($categoriesResult))->toArray();

        return view('admin.product.index', compact('products','categories', 'breadcrumbs'));
    }


    public function fetchData(Request $request)
    {


        $products = app('servicesV1')->productService->filterProducts($request)->paginate($this->dashboardPaginate);

        $products->getCollection()->transform(function ($item) use ($request) {
            return (new ProductResource($item))->toArray($request);
        });

        return view('admin.product.include.pagination_data', compact('products'))->render();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('product.index'), 'name' => __('Products')],
            ['name' => __('Add')]
        ];

        $categoriesResult = app('servicesV1')->categoryService->getAllCategoriesParent()->get();
        $categories = (new CategoryCollection($categoriesResult))->toArray();

//        dump($categories);die();
        return view('admin.product.create', compact('categories', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {

        $result = app('servicesV1')->productService->storeProduct($request);

        if ($result) {
            return redirect()->route('product.index')->with('success', __('message.success'));
        } else {
            return redirect()->route('product.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $result = app('servicesV1')->productService->getProductById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('product.index'), 'name' => __('Products')],
            ['name' => __('Show')],
            ['name' => "#" . $result->id . ' - ' . $result->name .' - ' . $result->created_at->format('m/d/Y')]
        ];

        $product = (new ProductResource($result))->toArray();

        return view('admin.product.show', compact('product', 'breadcrumbs'));

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $result = app('servicesV1')->productService->destroyProductById($id);

        if ($result) {
            return redirect()->route('product.index')->with('success', __('message.delete'));
        } else {
            return redirect()->route('product.index')->with('failed', __('message.failed'));
        }
    }


    public function export()
    {
        ob_end_clean();

        return Excel::download(new ProductExport(), __('Products') . '.xlsx');
    }
}
