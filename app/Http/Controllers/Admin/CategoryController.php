<?php

namespace App\Http\Controllers\Admin;

use App\Exports\CategoryExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\CategoryStoreRequest;
use App\Http\Requests\Admin\Category\CategoryUpdateRequest;
use App\Http\Resources\Dashboard\Category\CategoryCollection;
use App\Http\Resources\Dashboard\Category\CategoryResource;
use App\Models\Category\Category;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CategoryController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_category'])->only('index');
        $this->middleware(['permission:create_category'])->only('create');
        $this->middleware(['permission:update_category'])->only('edit');
        $this->middleware(['permission:delete_category'])->only('destroy');
        $this->middleware(['permission:read_category'])->only('export');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Categories')]
        ];

        $categories = app('servicesV1')->categoryService->getAllCategories()->paginate($this->dashboardPaginate);

        $categories->getCollection()->transform(function ($item) use ($request) {
            return (new CategoryResource($item))->toArray($request);
        });


        return view('admin.category.index', compact('categories', 'breadcrumbs'));
    }


    public function fetchData(Request $request)
    {


        $categories = app('servicesV1')->categoryService->filterCategories($request)->paginate($this->dashboardPaginate);

        $categories->getCollection()->transform(function ($item) use ($request) {
            return (new CategoryResource($item))->toArray($request);
        });

        return view('admin.category.include.pagination_data', compact('categories'))->render();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('category.index'), 'name' => __('Categories')],
            ['name' => __('Add')]
        ];

        $categoriesResult = app('servicesV1')->categoryService->getAllCategories()->get();
        $categories = (new CategoryCollection($categoriesResult))->toArray();

        return view('admin.category.create', compact('categories', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {

        $category = app('servicesV1')->categoryService->storeCategory($request);

        if ($category) {
            return redirect()->route('category.index')->with('success', __('message.success'));
        } else {
            return redirect()->route('category.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $result = app('servicesV1')->categoryService->getCategoryById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('category.index'), 'name' => __('Categories')],
            ['name' => __('Show')],
            ['name' => "#" . $result->id . ' - ' . $result->name .' - ' . $result->created_at->format('m/d/Y')]
        ];


        $category = (new CategoryResource($result))->toArray();

        return view('admin.category.show', compact('category', 'breadcrumbs'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $result = app('servicesV1')->categoryService->getCategoryById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('category.index'), 'name' => __('Categories')],
            ['name' => __('Edit')],
            ['name' => "#" . $result->id . ' - ' . $result->name .' - ' . $result->created_at->format('m/d/Y')]
        ];


        $category = (new CategoryResource($result))->toArray();
        $categoriesResult = app('servicesV1')->categoryService->getAllCategoriesExcpectId($id)->get();
        $categories =  (new CategoryCollection($categoriesResult))->toArray();


        return view('admin.category.edit', compact('category','categories', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, $id)
    {

        $result = app('servicesV1')->categoryService->updateCategory($request, $id);

        if ($result) {
            return redirect()->route('category.index')->with('success', __('message.update'));
        } else {
            return redirect()->route('category.index')->with('failed', __('message.failed'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $result = app('servicesV1')->categoryService->destroyCategoryById($id);

        if ($result) {
            return redirect()->route('category.index')->with('success', __('message.delete'));
        } else {
            return redirect()->route('category.index')->with('failed', __('message.failed'));
        }
    }


    public function export()
    {
        ob_end_clean();

        return Excel::download(new CategoryExport(), __('Categories') . '.xlsx');
    }
}
