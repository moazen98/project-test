<?php

namespace App\Http\Controllers\Admin\User;

use App\Exports\RoleExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\Role\RoleStoreRequest;
use App\Http\Requests\Admin\User\Role\RoleUpdateRequest;
use App\Http\Resources\Dashboard\Role\RoleCollection;
use App\Http\Resources\Dashboard\Role\RoleResource;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class RoleController extends MainDashboardController
{
    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_role'])->only('index');
        $this->middleware(['permission:create_role'])->only('create');
        $this->middleware(['permission:update_role'])->only('edit');
        $this->middleware(['permission:delete_role'])->only('destroy');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['name' => __('Roles')]
        ];

        $roles = Role::when($request->search, function ($q) use ($request) {
            return $q->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('name_ar', 'like', '%' . $request->search . '%')
                ->orWhere('description', 'like', '%' . $request->search . '%')
                ->orWhere('description_ar', 'like', '%' . $request->search . '%');
        })->latest()->paginate($this->dashboardPaginate);

        $roles->getCollection()->transform(function ($item) use ($request) {
            return (new RoleResource($item))->toArray($request);
        });


        return view('admin.user.role.index', compact('roles','breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => "/admin/users/role/index", 'name' => __('Roles')],
            ['name' => __('Add')]
        ];

        return view('admin.user.role.create',compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleStoreRequest $request)
    {


        $role = Role::firstOrCreate([
            'name' => $request->name,
            'name_ar' => $request->name_ar,
            'display_name' => $request->name,
            'description' => $request->description,
            'description_ar' => $request->description_ar
        ]);

        $permissions = [];


        foreach ($request->permissions as $permission) {

            $permissions[] = Permission::where('name','=',$permission)->first()->id;
        }

        $role->permissions()->sync($permissions);

        return redirect()->route('role.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => "/admin/users/role/index", 'name' => __('Roles')],
            ['name' => __('Show')]
        ];

        $role = Role::findOrFail($id);

        return view('admin.user.role.show',compact('role','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => "/admin/users/role/index", 'name' => __('Roles')],
            ['name' => __('Edit')]
        ];

        $role = Role::findOrFail($id);

        return view('admin.user.role.edit',compact('role','breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleUpdateRequest $request, $id)
    {

        $role = Role::findOrFail($id);
        $role->name = $request->name;
        $role->name_ar = $request->name_ar;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->description_ar = $request->description_ar;

        $role->save();

        $permissions = [];

        $role->permissions;

        foreach ($role->permissions as $rolePermissions){
            $role->detachPermission($rolePermissions);
        }



        foreach ($request->permissions as $permission) {
            $permissions[] = Permission::where('name','=',$permission)->first()->id;
        }


        $role->permissions()->sync($permissions);


        return redirect()->route('role.index')->with('success', __('message.update'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        $role->delete();

        return redirect()->route('role.index')->with('success', __('message.delete'));

    }

    public function export(){

        ob_end_clean();

        return Excel::download(new RoleExport(), __('Roles') . '.xlsx');
    }
}
