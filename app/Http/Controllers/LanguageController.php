<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LanguageController extends Controller
{


    public function swap($locale)
    {
        // available language in template array
        $availLocale = ['en' => 'en', 'ar' => 'ar', 'de' => 'de', 'pt' => 'pt'];
        // check for existing language
        if (array_key_exists($locale, $availLocale)) {
            session()->put('locale', $locale);
            if ($locale == 'ar') {
                config(['MIX_CONTENT_DIRECTION' => 'rtl']);
                session()->put('direction', 'rtl');
                session()->put('locale',$locale);
                session(["language" => 'ar']);
            } else {
                config(['MIX_CONTENT_DIRECTION' => 'ltr']);
                session()->put('direction', 'ltr');
                session()->put('locale',$locale);
                session(["language" => 'en']);
            }
        }

        return redirect()->back();
    }
}
