<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required|numeric|unique:authentications,phone',
            'international_code' => 'required',
            'email' => 'required|email|unique:authentications,email',
            'password' => 'required|min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required|min:6',
            'role' => 'required',
            'image' => 'mimes:jpg,jpeg,png',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => __('validation.required'),
            'last_name.required' => __('validation.required'),
            'section.required' => __('validation.required'),
            'phone.required' => trans('validation.required'),
            'phone.numeric' => trans('validation.numeric'),
            'phone.unique' => trans('validation.unique'),
            'international_code.required' => trans('validation.required'),
            'email.required' => __('validation.required'),
            'email.email' => __('validation.email'),
            'email.unique' => __('validation.unique'),
            'password.required' => __('validation.required'),
            'password.min' => __('validation.min'),
            'password.same' => __('validation.same'),
            'confirm_password.min' => __('validation.min'),
            'confirm_password.required' => __('validation.required'),
            'role.required' => __('validation.required'),
            'image.mimes' => __('validation.file'),
        ];
    }
}
