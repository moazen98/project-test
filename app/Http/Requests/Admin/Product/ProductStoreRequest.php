<?php

namespace App\Http\Requests\Admin\Product;

use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return RuleFactory::make([
            '%name%' => ['required'],
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
        ]);
    }

    public function attributes()
    {
        return RuleFactory::make([
            '%name%' => __('validation.attributes.name'),
        ]);
    }


    public function messages()
    {
        return [
            '*.name.required' => __('validation.required'),
            'price.required' => __('validation.required'),
            'price.numeric' => __('validation.numeric'),
            'stock.required' => __('validation.required'),
            'stock.numeric' => __('validation.numeric'),
        ];
    }
}
