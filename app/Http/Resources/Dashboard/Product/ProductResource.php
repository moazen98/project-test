<?php

namespace App\Http\Resources\Dashboard\Product;

use App\Http\Resources\Dashboard\Category\CategoryCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'stock' => $this->stock,
            'categories' => $this->categories()->count() == 0 ? null : (new CategoryCollection($this->categories))->toArray(),
            'categories_number' => $this->categories()->count()
        ];
    }

    public function withDetails($request=null)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'stock' => $this->stock,
            'categories_number' => $this->categories()->count()
        ];
    }

    public function withDetailsLess($request=null)
    {
        return [
            'name' => $this->name,
        ];
    }
}
