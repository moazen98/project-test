<?php

namespace App\Http\Resources\Dashboard\Product;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'products' => $this->collection->map(function ($product) use ($request) {
                return (new ProductResource($product))->toArray($request);
            })
        ];
    }

    public function toArrayWithDetails($request=null)
    {
        return [
            'products' => $this->collection->map(function ($product) use ($request) {
                return (new ProductResource($product))->withDetails($request);
            })
        ];
    }

    public function toArrayWithDetailsLess($request=null)
    {
        return [
            'products' => $this->collection->map(function ($product) use ($request) {
                return (new ProductResource($product))->withDetailsLess($request);
            })
        ];
    }
}
