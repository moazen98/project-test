<?php

namespace App\Http\Resources\Dashboard\Agency\User;

use App\Enums\AccountStatus;
use App\Enums\AgencyType;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'name' => $this->first_name.' '.$this->last_name,
            'full_name' => $this->name,
            'role' => $this->roles()->count() == 0 ? __('No data') : (app()->getLocale() == 'ar' ? $this->roles()->first()->name_ar : $this->roles()->first()->name),
            'role_object' => $this->roles()->count() == 0 ? null : $this->roles()->first(),
            'role_id' => $this->roles()->count() == 0 ? null : $this->roles()->first()->id,
            'phone' => $this->authentication == null ? null : $this->authentication->phone,
            'international_code' => $this->authentication == null ? null : $this->authentication->international_code,
            'full_phone' => $this->full_phone,
            'status_class' => $this->authentication == null ? null :  getUserStatusString($this->authentication->is_active ? AccountStatus::ACTIVE : AccountStatus::NOT_ACTIVE),
            'email' => $this->authentication == null ? null : $this->authentication->email,
            'active' => $this->authentication == null ? null : $this->authentication->is_active,
            'auth_id' => $this->authentication == null ? null : $this->authentication->id,
            'active_string' => $this->authentication == null ? null : ($this->authentication->is_active == 1 ? __('Active') : __('InActivate')),
            'image_path' => $this->image_path,
            'user_type' => AgencyType::USERS,
            'basic_user' => $this->basic_user,
            'user_type_string' => __('User'),
        ];
    }

}
