<?php

namespace App\Http\Resources\Dashboard\Agency\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserSettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'type' => $this->settingable_type,
            'is_turkey' => $this->is_turkey,
            'is_dollar' => $this->is_dollar,
            'is_euro' => $this->is_euro,
            'is_gold' => $this->is_gold,
            'is_change_id' => $this->is_change_id,
            'is_show_name' => $this->is_show_name,
            'is_depended' => $this->is_depended,
            'created_at' => $this->created_at->format('Y-m-d'),
        ];
    }
}
