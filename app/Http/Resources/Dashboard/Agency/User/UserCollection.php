<?php

namespace App\Http\Resources\Dashboard\Agency\User;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'users' => $this->collection->map(function ($user) use ($request) {
                return (new UserResource($user))->toArray($request);
            })
        ];
    }


    public function mainDetailsUser($request=null)
    {
        return [
            'users' => $this->collection->map(function ($user) use ($request) {
                return (new UserResource($user))->mainDetails($request);
            })
        ];
    }
}
