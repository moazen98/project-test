<?php

namespace App\Http\Resources\Dashboard\Agency\File;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserFilesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'files' => $this->collection->map(function ($file) use ($request) {
                return (new UserFilesResource($file))->toArray($request);
            })
        ];
    }
}
