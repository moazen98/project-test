<?php

namespace App\Http\Resources\Dashboard\Category;

use App\Http\Resources\Dashboard\Product\ProductCollection;
use App\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'category' => $this,
            'category_parent' => $this->parentCategory == null ? null : $this->wtihCategory($this->parentCategory),
            'children' =>  $this->categories()->count() == 0 ? null : (new CategoryCollection($this->categories))->withCategoryChildren($request),
            'products' =>  $this->products()->count() == 0 ? null : (new ProductCollection($this->products))->toArrayWithDetails($request),
            'products_number' => $this->products()->count()
        ];
    }


    public function wtihCategory($category)
    {
        return [
            'id' => $category->id,
            'name' => $category->name,
            'children' =>  $category->categories()->count() == 0 ? null : (new CategoryCollection($category->categories))->withCategoryChildren(),
        ];
    }
}
