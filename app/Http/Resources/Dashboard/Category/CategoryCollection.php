<?php

namespace App\Http\Resources\Dashboard\Category;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'categories' => $this->collection->map(function ($category) use ($request) {
                return (new CategoryResource($category))->toArray($request);
            })
        ];
    }


    public function withCategoryChildren($request=null)
    {
        return [
            'categories' => $this->collection->map(function ($category) use ($request) {
                return (new CategoryResource($category))->wtihCategory($category);
            })
        ];
    }
}
