<?php

namespace App\Http\Middleware;

use App\Models\Programme;
use Closure;
use Illuminate\Http\Request;

class RemovePrescriptionEmpty
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $allProgramme = Programme::all();
        foreach ($allProgramme as $programme) {
            foreach ($programme->prescriptions as $prescription) {
                if (count($prescription->services) == 0) {
                    $prescription->delete();
                }
            }
        }
        return $next($request);
    }
}
