<?php


use App\Enums\AccountStatus;
use App\Enums\MediaExtension;
use App\Enums\UserAuthenticationType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

function upload_files($upload_path, $folder, $allowed_types = 'jpeg|jpg|png', $request)
{


    $ret_files = array();

    $upload_path = $upload_path . $folder;


    $config['upload_path'] = $upload_path;


    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);

//        touch($upload_path . DIRECTORY_SEPARATOR . 'index.html');
    }

    $uploaded_file = null;

    foreach ($request->image as $file_key => $file) {

        $imageName = $file->getClientOriginalName();

        $uploaded_file[$file_key] = $file->move(public_path($upload_path), $imageName);
    }

    $res = is_null($uploaded_file);

    if ($res != true) {

        foreach ($request->image as $file_key => $file) {

            $ret_files[$file_key]['file_name'] = $file->getClientOriginalName();
            $ret_files[$file_key]['file_type'] = $file->getClientOriginalExtension();

            $imageName = $file->getClientOriginalName();

            $ret_files[$file_key]['real_path'] = $folder . '/' . $imageName;

        }

    } else {
        $ret_files['file_key'] = 'error upload';
        $response['status'] = false;
        $response['ret_data'] = array();

        return $response;
    }

    return $ret_files;
}


function upload_icons($upload_path, $folder, $allowed_types = 'jpeg|jpg|png', $request)
{


    $ret_files = array();

    $upload_path = $upload_path . $folder;


    $config['upload_path'] = $upload_path;


    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);

//        touch($upload_path . DIRECTORY_SEPARATOR . 'index.html');
    }

    $uploaded_file = null;

    foreach ($request->icon as $file_key => $file) {

        $imageName = $file->getClientOriginalName();

        $uploaded_file[$file_key] = $file->move(public_path($upload_path), $imageName);
    }

    $res = is_null($uploaded_file);

    if ($res != true) {

        foreach ($request->icon as $file_key => $file) {

            $ret_files[$file_key]['file_name'] = $file->getClientOriginalName();
            $ret_files[$file_key]['file_type'] = $file->getClientOriginalExtension();

            $imageName = $file->getClientOriginalName();

            $ret_files[$file_key]['real_path'] = $folder . '/' . $imageName;

        }

    } else {
        $ret_files['file_key'] = 'error upload';
        $response['status'] = false;
        $response['ret_data'] = array();

        return $response;
    }

    return $ret_files;
}


function upload_filesServicePrescriptions($upload_path, $folder, $allowed_types = 'jpeg|jpg|png', $type, $servicePrescriptionId, $request)
{

    $ret_files = array();

    $upload_path = $upload_path . $folder;


    $config['upload_path'] = $upload_path;

    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);
    }


    $full_path = $upload_path;

    if ($type == 'category') {
        if (!file_exists($upload_path . '/' . 'category') && !file_exists(public_path() . '/' . $upload_path . '/' . 'category')) {

            File::makeDirectory(public_path() . '/' . $upload_path . '/' . 'category', 0777, true);

        }
        $full_path .= '/' . 'category';
    }


    if ($type == 'item') {
        if (!file_exists($upload_path . '/' . 'item') && !file_exists(public_path() . '/' . $upload_path . '/' . 'item')) {
            File::makeDirectory(public_path() . '/' . $upload_path . '/' . 'item', 0777, true);
        }
        $full_path .= '/' . 'item';
    }

    if ($type == 'category') {
        if (!file_exists($upload_path . '/' . 'category' . '/' . 'service_prescription_' . $servicePrescriptionId) && !file_exists(public_path() . '/' . $upload_path . '/' . 'category' . '/' . 'service_prescription_' . $servicePrescriptionId)) {
            File::makeDirectory(public_path() . '/' . $upload_path . '/' . 'category' . '/' . 'service_prescription_' . $servicePrescriptionId, 0777, true);
        }
        $full_path .= '/' . 'service_prescription_' . $servicePrescriptionId;
    }

    if ($type == 'item') {
        if (!file_exists($upload_path . '/' . 'item' . '/' . 'service_prescription_' . $servicePrescriptionId) && !file_exists(public_path() . '/' . $upload_path . '/' . 'item' . '/' . 'service_prescription_' . $servicePrescriptionId)) {
            File::makeDirectory(public_path() . '/' . $upload_path . '/' . 'item' . '/' . 'service_prescription_' . $servicePrescriptionId, 0777, true);
        }
        $full_path .= '/' . 'service_prescription_' . $servicePrescriptionId;
    }

    $uploaded_file = null;

    foreach ($request->image as $file_key => $file) {

        $imageName = $file->getClientOriginalName();

        $uploaded_file[$file_key] = $file->move(public_path($full_path), $imageName);
    }

    $res = is_null($uploaded_file);

    if ($res != true) {

        foreach ($request->image as $file_key => $file) {

            $ret_files[$file_key]['file_name'] = $file->getClientOriginalName();
            $ret_files[$file_key]['file_type'] = $file->getClientOriginalExtension();

            $imageName = $file->getClientOriginalName();


            $ret_files[$file_key]['real_path'] = $full_path . '/' . $imageName;

        }

    } else {
        $ret_files['file_key'] = 'error upload';
        $response['status'] = false;
        $response['ret_data'] = array();

        return $response;
    }

    return $ret_files;
}


function upload_single_files($upload_path, $folder, $allowed_types = 'png|jpg|jpeg|pdf|xls', $request)
{


    $size = $request->getSize();
    $ret_files = array();

    $upload_path = $upload_path;


    $config['upload_path'] = $upload_path;


    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);

    }

    $uploaded_file = null;

    $imageName = $request->getClientOriginalName();
    $imageNameHash = $request->hashName();
    $imageExtension = $request->getClientOriginalExtension();

    $uploaded_file[0] = $request->move(public_path($upload_path), $imageNameHash);

    $res = is_null($uploaded_file);

    if ($res != true) {

        $ret_files[0]['size'] = formatBytes($size);
        $ret_files[0]['file_name'] = $imageNameHash;
        $ret_files[0]['real_name'] = $imageName;
        $ret_files[0]['file_type'] = $imageExtension;
        $ret_files[0]['real_path'] = $imageName;


    } else {
        $ret_files['file_key'] = 'error upload';
        $response['status'] = false;
        $response['ret_data'] = array();

        return $response;
    }

    return $ret_files;
}

function week_of_month()
{

    return ceil(Carbon::now()->month(Carbon::now()->month)->daysInMonth / 7);
}

function getWeekday($date)
{
    return ceil(date('d', strtotime($date)) / 7);
}


function mobile_user()
{
    return null;
}


function formatBytes($bytes, $precision = 2)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');


    $bytes = max($bytes, 0);
    $pow = ceil($bytes / 1048576);

    // Uncomment one of the following alternatives
    // $bytes /= pow(1024, $pow);
    // $bytes /= (1 << (10 * $pow));

//    return round($bytes, $precision) . ' ' . $units[$pow];
    return round($pow, $precision);
}


function requestConvert($request)
{
    $requestConvert = new Request();
    $requestConvert = $request;

    return $requestConvert;
}

function image_extensions()
{

    $extensions_images = array();
    $extensions_images = MediaExtension::getValues(['PNG', 'JPEG', 'JPG', 'GIF']);

    return $extensions_images;
}


function files_extensions()
{

    $extensions_files = array();
    $extensions_files = MediaExtension::getValues(['XLS', 'PDF']);

    return $extensions_files;
}

function audio_extensions()
{

    $extensions_audio = array();
    $extensions_audio = MediaExtension::getValues(['MP3', 'OGG', 'WAV']);

    return $extensions_audio;
}

function vedio_extensions()
{

    $extensions_vedio = array();
    $extensions_vedio = MediaExtension::getValues(['MP4', 'MPEG', 'MPGA']);

    return $extensions_vedio;
}

//TODO : put other users when you create them
function getCurrentDashboardUser()
{

    $user = \Illuminate\Support\Facades\Auth::user();
    return $user;

}


function checkIfAvailableOperation($received)
{

    if ($received) {
        return false;
    }
    return true;

}

function getUserAgencyType($type)
{
    if ($type->authentication) {
        return $type->authentication->authenticatable_type;
    }

    return null;
}


function CheckUserVerified($type, $user)
{

    if ($user->authentication) {
        return $user->authentication->is_verified;
    }

    return false;
}


function CheckUserStatus($type, $user)
{

    if ($user->authentication) {
        return $user->authentication->is_active;
    }

    return false;

}




function inputCredentialType($request)
{
    $method = $request->input('credential') == null ? null : (filter_var($request->input('credential'), FILTER_VALIDATE_EMAIL) ? UserAuthenticationType::EMAIL : (is_numeric($request->input('credential')) ? UserAuthenticationType::PHONE : UserAuthenticationType::OTHER));

    return $method;

}

function getUserStatusString($type)
{

    if (AccountStatus::ACTIVE == $type) {
        return "success";
    }

    if (AccountStatus::NOT_ACTIVE == $type) {
        return "danger";
    }


    if (AccountStatus::VERIFY == $type) {
        return "info";
    }

    if (AccountStatus::NOT_VERIFY == $type) {
        return "warning";
    }

    return null;
}


function getUserAuthGuard()
{

    if (Auth::guard('web')->check()) {
        $user = Auth::guard('web')->user();
        return $user;
    } elseif (Auth::guard('office')->check()) {
        $user = Auth::guard('office')->user();
        return $user;
    } elseif (Auth::guard('brunch')->check()) {
        $user = Auth::guard('brunch')->user();
        return $user;
    } elseif (Auth::guard('dealership')->check()) {
        $user = Auth::guard('dealership')->user();
        return $user;
    }

    return null;
}



