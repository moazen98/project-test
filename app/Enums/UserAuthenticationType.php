<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class UserAuthenticationType extends Enum
{
    const EMAIL =   'email';
    const PHONE =   'phone';
    const OTHER = 'other';
}
