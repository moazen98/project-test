<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class AgencyType extends Enum
{
    const USERS =   1;
    const COMPANIES =   2;
    const EMPLOYEES =   3;
}
