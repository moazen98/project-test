<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class AccountStatus extends Enum
{
    const ACTIVE =   2;
    const NOT_ACTIVE =   3;
    const VERIFY= 4;
    const NOT_VERIFY= 5;
}
