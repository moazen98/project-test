<?php

namespace App\Observers;

use App\Models\Product\Product;
use App\Models\ProductCategory;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ProductObserver
{
    public function deleting(Product $product)
    {
        DB::beginTransaction();

        $productCategories = ProductCategory::where('product_id',$product->id)->delete();

        DB::commit();
    }
}
