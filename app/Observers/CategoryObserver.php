<?php

namespace App\Observers;

use App\Models\Category\Category;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\DB;

class CategoryObserver
{

    public function deleting(Category $category)
    {
        DB::beginTransaction();

        $productCategories = ProductCategory::where('category_id',$category->id)->delete();

        DB::commit();
    }
}
