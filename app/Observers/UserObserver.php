<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserObserver
{
    public function deleting(User $user)
    {
        DB::beginTransaction();
        if ($user->authentication) {
            $user->authentication->email = $user->email . '-' . $user->id . '-deleted';
            $user->authentication->phone = $user->phone . '-' . $user->id . '-deleted';
            $user->save();
        }
        DB::commit();
    }
}
