<?php

namespace App\Services;

use App\Enums\AccountStatus;
use App\Enums\AgencyType;
use App\Models\Authentication;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * Class AuthenticationService.
 */
class AuthenticationService extends MainDashboardService
{


    public function getUserMobileByEmail($email)
    {

        $credential = Authentication::where('email', $email)->first();


        if (!$credential) {
            return null;
        }

        $user = $credential->authenticatable;

        return $user;

    }


    public function getAgencyValue()
    {

        $data = AgencyType::getValues('OFFICE', 'BRANCH', 'DEALERSHIP');

        return $data;

    }

    public function getUserMobileByPhone($phone)
    {

        $credential = Authentication::where('phone', $phone)->first();


        if (!$credential) {
            return null;
        }

        $user = $credential->authenticatable;

        return $user;
    }



    public function userRegister($user, $fcm)
    {


    }

    public function loginViaEmail($email, $password)
    {

        if ($user = Authentication::where('email', $email)->first()) {


            if (!$user->is_active) {
                return AccountStatus::NOT_ACTIVE;
            }

            if (!$user->is_verified) {
                return AccountStatus::NOT_VERIFY;
            }


            if (Hash::check($password, $user->password)) {
                return $this->login($user->authenticatable->id, $user->authenticatable_type);
            }

            return 0;
        }

        return 0;
    }


    public function loginViaPhone($phone, $password)
    {
        if ($user = Authentication::where('phone', $phone)->first()) {


            if (!$user->is_active) {
                return AccountStatus::NOT_ACTIVE;
            }

            if (!$user->is_verified) {
                return AccountStatus::NOT_VERIFY;
            }


            if (Hash::check($password, $user->password)) {
                return $this->login($user->authenticatable->id, $user->authenticatable_type);
            }

            return 0;

        }

        return 0;
    }


    public function login($id, $type)
    {

        if ($type == User::class) {

            Auth::guard('web')->loginUsingId($id);
            return 1;
        }

        return 0;

    }


    public function logout()
    {

        if (Auth::guard('web')->check()) {
            Auth::guard('web')->logout();
            return AgencyType::USERS;
        }

        return false;

    }



}
