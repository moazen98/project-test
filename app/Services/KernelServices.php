<?php

namespace App\Services;

/**
 * Class KernelServices.
 */
class KernelServices
{

    public $authenticationService;
    public $userService;
    public $categoryService;
    public $productService;


    /**
     * KernelServices constructor.
     */
    public function __construct()
    {
        $this->authenticationService = new AuthenticationService();
        $this->userService = new UserService();
        $this->categoryService = new CategoryService();
        $this->productService = new ProductService();
    }
}
