<?php

namespace App\Services;

use App\Models\Category\Category;
use App\Models\Product\Product;
use Illuminate\Support\Facades\DB;

/**
 * Class ProductService.
 */
class ProductService extends MainDashboardService
{


    public function getAllProducts()
    {

        $products = Product::query()->latest();

        return $products;
    }

    public function getAllProductsExpectId($id)
    {

        $products = Product::query()->where('id', '!=', $id)->latest();

        return $products;
    }



    public function storeProduct($request)
    {

//        try {
            DB::beginTransaction();

            $product = new Product();
            $product->setTranslatedAttributes($request);
            $product->price = $request->price;
            $product->stock = $request->stock;

            $product->save();


        if ($request->category) {
            foreach ($request->category as $cate){
                $product->categories()->attach($cate);
            }
        }

            DB::commit();

            return true;

//        } catch (\Exception $exception) {
//
//            DB::rollBack();
//
//            return false;
//        }

    }


    public function updateProduct($request, $id)
    {

        try {


            DB::beginTransaction();

            $product = Product::query()->findOrFail($id);
            $product->setTranslatedAttributes($request);
            $product->price = $request->price;
            $product->stock = $request->stock;

            if ($request->category_id) {
                $parent = Category::findOrFail($request->category_id);
                $product->parent()->associate($parent);
            }

            $product->save();

            DB::commit();

            return true;


        } catch (\Exception $exception) {

            DB::rollBack();

            return false;
        }

    }


    public function getProductById($id)
    {
        $product = Product::query()->findOrFail($id);

        return $product;
    }

    public function destroyProductById($id)
    {

        DB::beginTransaction();

        //TODO: there is observer for it

        $product = Product::query()->findOrFail($id);
        $product->delete();

        DB::commit();

        return true;
    }


    public function filterProducts($request)
    {

        $products = Product::query();

        $dataTableFilter = $request->all();


        if ($dataTableFilter['query']) {
            $search = $request->get('query');
            $products->where(function ($query) use ($search) {
                $query->whereTranslationLike('name', '%' . $search . '%')
                ->orWhere('id',$search);
            });
        }

        if ($dataTableFilter['category_id']) {

            $category = $request->get('category_id');

            $products->whereHas('categories', function ($query) use ($category) {
                $query->where('categories.id',$category);
            });

        }

        $orderBy = $dataTableFilter['sorttype'] ? $dataTableFilter['sorttype'] : 'DESC';
        $products = $products->orderBy('id', $orderBy);

        return $products;
    }


    public function getProductName($productsResult){

        $resultName = [];
        foreach ($productsResult as $index => $item){
            $resultName[$index] = $item->name;
        }

        return $resultName;
    }

    public function getProductNumber($productsResult){

        $resultNumber = [];
        foreach ($productsResult as $index => $item){
            $resultNumber[$index] = $item->stock;
        }

        return $resultNumber;
    }
}
