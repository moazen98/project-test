<?php

namespace App\Services;

use App\Models\Category\Category;
use Illuminate\Support\Facades\DB;

/**
 * Class CategoryService.
 */
class CategoryService extends MainDashboardService
{


    public function getAllCategories()
    {

        $categories = Category::query()->latest();

        return $categories;
    }

    public function getAllCategoriesParent()
    {

        $categories = Category::query()->whereDoesntHave("parentCategory")->latest();

        return $categories;
    }

    public function getAllCategoriesExcpectId($id)
    {

        $categories = Category::query()->where('id', '!=', $id)->latest();

        return $categories;
    }


    public function storeCategory($request)
    {

        try {
            DB::beginTransaction();

            $category = new Category();
            $category->setTranslatedAttributes($request);
            if ($request->category_id) {
                $parent = Category::findOrFail($request->category_id);
                $category->parentCategory()->associate($parent);
            }
            $category->save();

            DB::commit();

            return true;

        } catch (\Exception $exception) {

            DB::rollBack();

            return false;
        }

    }


    public function updateCategory($request, $id)
    {

        try {


            DB::beginTransaction();

            $category = Category::query()->findOrFail($id);
            $category->setTranslatedAttributes($request);

            if ($request->category_id) {
                $parent = Category::findOrFail($request->category_id);
                $category->parentCategory()->associate($parent);
            }

            $category->save();

            DB::commit();

            return true;


        } catch (\Exception $exception) {

            DB::rollBack();

            return false;
        }

    }


    public function getCategoryById($id)
    {
        $category = Category::query()->findOrFail($id);

        return $category;
    }

    public function destroyCategoryById($id)
    {

        DB::beginTransaction();

        //TODO: there is observer for it

        $category = Category::query()->findOrFail($id);
        $category->delete();

        DB::commit();

        return true;
    }


    public function filterCategories($request)
    {

        $categories = Category::query();

        $dataTableFilter = $request->all();


        if ($dataTableFilter['query']) {
            $search = $request->get('query');
            $categories->where(function ($query) use ($search) {
                $query->whereTranslationLike('name', '%' . $search . '%')
                    ->orWhere('id',$search);
            });
        }

        $orderBy = $dataTableFilter['sorttype'] ? $dataTableFilter['sorttype'] : 'DESC';
        $categories = $categories->orderBy('id', $orderBy);

        return $categories;
    }
}
