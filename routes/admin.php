<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\StatisticController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\DashboardController;
use \App\Http\Controllers\LanguageController;
use \App\Http\Controllers\Admin\Authentication\AuthController;
use \App\Http\Controllers\Admin\User\UserController;
use \App\Http\Controllers\Admin\User\RoleController;

Route::post('login', [AuthController::class, 'login'])->name('admin.login');
Route::get('login', [AuthController::class, 'getAdminLogin'])->name('admin.get.login');
Route::get('/', [AuthController::class, 'index'])->name('admin.get.index');


Route::group(['middleware' => ['auth']], function () {

    Route::get('/dashboard', [DashboardController::class, 'dashboardEcommerce'])->name('admin.dashboard-ecommerce');
    Route::get('/logout', [AuthController::class, 'logout'])->name('admin.logout');


    Route::group(['prefix' => 'users'], function () {

        Route::group(['prefix' => 'dashboard'], function () {


            Route::get('index', [UserController::class, 'index'])->name('user.index');
            Route::get('/pagination/fetch-data', [UserController::class, 'fetchData'])->name('user.fetchData');
            Route::get('create', [UserController::class, 'create'])->name('user.create');
            Route::get('show/{id}', [UserController::class, 'show'])->name('user.show');
            Route::get('edit/{id}', [UserController::class, 'edit'])->name('user.edit');
            Route::post('update/{id}', [UserController::class, 'update'])->name('user.update');
            Route::post('store', [UserController::class, 'store'])->name('user.store');
            Route::delete('delete/{id}', [UserController::class, 'destroy'])->name('user.destroy');
            Route::get('profile/{id}', [UserController::class, 'profile'])->name('user.profile');
            Route::get('export', [UserController::class, 'export'])->name('user.export');

        });

        Route::group(['prefix' => 'role'], function () {

            Route::get('index', [RoleController::class, 'index'])->name('role.index');
            Route::get('create', [RoleController::class, 'create'])->name('role.create');
            Route::get('show/{id}', [RoleController::class, 'show'])->name('role.show');
            Route::get('edit/{id}', [RoleController::class, 'edit'])->name('role.edit');
            Route::post('update/{id}', [RoleController::class, 'update'])->name('role.update');
            Route::post('store', [RoleController::class, 'store'])->name('role.store');
            Route::delete('delete/{id}', [RoleController::class, 'destroy'])->name('role.destroy');
            Route::get('export', [RoleController::class, 'export'])->name('role.export');

        });


    });


    Route::group(['prefix' => 'category'], function () {

        Route::get('index', [CategoryController::class, 'index'])->name('category.index');
        Route::get('create', [CategoryController::class, 'create'])->name('category.create');
        Route::get('show/{id}', [CategoryController::class, 'show'])->name('category.show');
        Route::get('edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
        Route::post('update/{id}', [CategoryController::class, 'update'])->name('category.update');
        Route::post('store', [CategoryController::class, 'store'])->name('category.store');
        Route::delete('delete/{id}', [CategoryController::class, 'destroy'])->name('category.destroy');
        Route::get('export', [CategoryController::class, 'export'])->name('category.export');
        Route::get('/pagination/fetch-data', [CategoryController::class, 'fetchData'])->name('category.fetchData');

    });


    Route::group(['prefix' => 'product'], function () {

        Route::get('index', [ProductController::class, 'index'])->name('product.index');
        Route::get('create', [ProductController::class, 'create'])->name('product.create');
        Route::get('show/{id}', [ProductController::class, 'show'])->name('product.show');
        Route::get('edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
        Route::post('update/{id}', [ProductController::class, 'update'])->name('product.update');
        Route::post('store', [ProductController::class, 'store'])->name('product.store');
        Route::delete('delete/{id}', [ProductController::class, 'destroy'])->name('product.destroy');
        Route::get('export', [ProductController::class, 'export'])->name('product.export');
        Route::get('/pagination/fetch-data', [ProductController::class, 'fetchData'])->name('product.fetchData');

    });

    Route::group(['prefix' => 'statistics'], function () {
        Route::get('index', [StatisticController::class, 'index'])->name('statistic.index');

    });


    Route::get('lang/{locale}', [LanguageController::class, 'swap'])->name('lang.swap');

});

